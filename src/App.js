import React, { Suspense, lazy, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import customerStore from './stores/customer.store';

const Customers = lazy(() => import('./pages/Customers'));
const CustomerDetail = lazy(() => import('./pages/CustomerDetail'));

function App() {
  const fetchData = customerStore((state) => state.fetch);

  useEffect(() => {
      fetchData()
  }, [fetchData])
  return (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          <Route path="/" element={<Customers />} />
          <Route path="/:id" element={<CustomerDetail />} />
        </Routes>
      </Suspense>
    </Router>
  );
}

export default App;
