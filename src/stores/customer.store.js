import create from "zustand";
import axios from "axios";

const customerStore = create((set, get) => ({
  data: null,
  detail: null,
  loading: false,
  loadingDetail: false,
  hasErrors: false,
  fetch: async () => {
    set(() => ({ loading: true }));
    try {
      const response = await axios.get(
        `https://gorest.co.in/public/v1/users`
      );
      set((state) => ({ data: (state.data = response.data), loading: false }));
    } catch (err) {
      set(() => ({ hasErrors: true, loading: false }));
    }
  },
  fetchDetail: async (id) => {
    set(() => ({ loadingDetail: true }));
    try {
      const response = await axios.get(
        `https://gorest.co.in/public/v1/users/${id}`
      );
      set((state) => ({ detail: (state.detail = response.data.data), loadingDetail: false }));
    } catch (err) {
      set(() => ({ hasErrors: true, loading: false }));
    }
  },
  deleteData: async (id, navigate) => {
    set(() => ({ loadingDetail: true }));
    const store = get((state) => state.data)
    let findIndex = store.data['data'].findIndex((val) => val.id == id);
    if (findIndex >= 0) {
        store.data['data'].splice(findIndex, 1)
        set((state) => ({ data: (state.data = store.data), loading: false }));
    }
    navigate('/')
  },
}));

export default customerStore;