import React, { useEffect } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import customerStore from '../stores/customer.store';

export default function CustomerDetail() {
    const navigate = useNavigate();
    const { id } = useParams()

    const data = customerStore((state) => state.detail);
    const fetchData = customerStore((state) => state.fetchDetail);
    const deleteData = customerStore((state) => state.deleteData);

    useEffect(() => {
        fetchData(id)
    }, [fetchData, id])

    return (
        <>
            <h2 className="my-4 text-center">CUSTOMER DETAILS</h2>
            {
                data && (
                    <Container>
                        <Row>
                            <Col md={6}>
                                <div className='mb-3'>
                                    <Form.Label>Full Name</Form.Label>
                                    <Form.Control readOnly type="text" value={data.name} />
                                </div>
                                <div className='mb-3'>
                                    <Form.Label>AccountID</Form.Label>
                                    <Form.Control readOnly type="text" value={`@${data.name.toLowerCase().replace(' ', '_')}`} />
                                </div>
                                <div className='mb-3'>
                                    <Form.Label>Gender</Form.Label>
                                    <Form.Control readOnly type="text" value={data.gender} />
                                </div>
                                <div className='mb-3'>
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control readOnly type="email" value={data.email} />
                                </div>
                            </Col>
                            <Col md={6}>
                                <div>
                                    <div className='mb-3'>
                                        <Row>
                                            <Col md={12}>
                                                <Form.Label>Status Account</Form.Label>
                                            </Col>
                                            <Col md={12}>
                                                <Button disabled className='w-100 text-capitalize font-weight-bold' variant={data.status === 'active' ? 'outline-success' : 'outline-danger'}>{data.status}</Button>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className='mb-3'>
                                        <Button className='w-100 mb-2' variant="primary" onClick={() => {
                                            navigate('/')
                                        }}>RETURN TO LIST CUSTOMER</Button>
                                        <Button className='w-100' variant="danger" onClick={() => {
                                            deleteData(data.id, navigate)
                                        }}>DELETE CUSTOMER</Button>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                )
            }
        </>
    )
}
