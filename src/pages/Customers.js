import { useState, useEffect } from "react";
import { Button, Table, Card, Container, Row, Col } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import customerStore from "../stores/customer.store";

function Customers() {
    const navigate = useNavigate();

    const data = customerStore((state) => state.data);

    return (
        <>
            <Container>
                <Row>
                    <Col md={12}>
                        <h2 className="my-4 text-center">LIST CUSTOMER</h2>
                    </Col>
                    <Col md={12}>
                        <table width="100%" className="custom-table">
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Status</th>
                            </tr>
                            {data &&
                                data.data.map((row, i) => (
                                    <tr key={row.id} className="bordered my-3">
                                        <td className="text-nowrap">{row.name}</td>
                                        <td>{row.email}</td>
                                        <td>{row.gender}</td>
                                        <td>{row.status}</td>
                                        <td>
                                            <Button variant="primary" size="sm" onClick={() => {
                                                navigate(`/${row.id}`)
                                            }}>View Cust</Button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </table>
                    </Col>
                </Row>
            </Container>

            <style scoped="jsx">{`
                .custom-table .bordered {
                    border: 1px solid black;
                }
                .custom-table td {
                    padding: 10px;
                }
                .custom-table th {
                    padding: 10px;
                }
            `}</style>
        </>
    );
}

export default Customers;
